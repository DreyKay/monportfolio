﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MonPortfolio.Models;

namespace MonPortfolio.Controllers
{
    public class PageController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Confidentialite()
        {
            return View();
        }
        public IActionResult Moi()
        {
            return View();
        }
        public IActionResult MonEtablissement()
        {
            return View();
        }
        public IActionResult MaFormation()
        {
            return View();
        }
        public IActionResult MesCompetences()
        {
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
